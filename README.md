# Full Stack Shu

Shuhari (Kanji: 守破離 Hiragana: しゅはり) is a Japanese martial art concept which describes the stages of learning to mastery.

shu (守) "protect", "obey"—traditional wisdom—learning fundamentals, techniques, heuristics, proverbs

This repo contains JZ's plan for acquiting SHU on the art of software development.

Technologies on my choice of stack:

- C#
- ASP.NET Core
- EF core
- Postgres
- React
- React Native
- Bash
- Rest API design
- XUnit
- F#
- Linux
- Distributed Systems
- Docker
- Kubernetes
- Gitlab
- Microsoft azure
- NGINX
